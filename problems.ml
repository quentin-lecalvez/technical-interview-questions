# Problèmes d'algo

## FizzBuzz
Pour tous les nombres de 1 à 100, afficher "Fizz" si le nombre est un multiple de 3, "Buzz" si le nombre est un multiple de 5 et "FizzBuzz" si le nombre est un multiple de 15. Sinon, afficher le nombre.

## Fibonacci
Calculer les valeurs de la suite de Fibonacci inférieur à n.
Fib(n+2) = Fib(n) + Fib(n+1)

## Nombre premier
Vérifier si un nombre est premier ou non.

## Anagramme
Vérifier si deux chaines de caractère sont des anagrammes ou non. Exemple d'anagrammes : chien et niche.

## Retirer les doubles
Étant donné une chaine de caractère, retirer les caractères ayant plusieurs occurrences en gardant seulement la première.
Par exemple, pour la chaine "ceci est un test" le résultat est "cei stun".

## Trouver le nombre manquant
Soit une liste de nombres entier et une seconde liste faite des mêmes nombres mélangés et avec un nombre en moins, trouver quel est le nombre qui a été retiré dans second liste.

## Première occurrence unique
Trouver la première occurrence unique d'un caractère dans un texte.

## Vérifier le parenthésage
Soit une chaine faite des caractères suivant "(){}[]", vérifier que le parenthésage est équilibré.

## Trouver les paires
Soit une liste de nombre entier, trouver toutes les paires de nombre ayant une somme égale à k.

## Trouver le prochain nombre formé des mêmes chiffres
Soit un nombre entier, trouver le prochain nombre formé des mêmes chiffres. Par exemple, pour 1234 le nombre suivant formé des mêmes chiffres est 1243.
