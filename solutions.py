import collections


def fizzBuzz():
    for i in range(1, 101):
        res = ""
        if i % 3 == 0:
            res += "Fizz"
        if i % 5 == 0:
            res += "Buzz"
        if res == '':
            res = i
        print(res)


def fib(n):
    a, b = 0, 1
    while a < n:
        print(a, end=' ')
        a, b = b, a + b
    print()


def isPrime(n):
    if n == 2 or n == 3:
        return True
    if n % 2 == 0 or n < 2:
        return False
    for i in range(3, int(n**0.5) + 1, 2):
        if n % i == 0:
            return False
    return True


def isAnagram1(str1, str2):
    return sorted(str1) == sorted(str2)


def isAnagram2(str1, str2):
    if len(str1) != len(str2):
        return False
    counts = collections.defaultdict(int)
    for letter in str1:
        counts[letter] += 1
    for letter in str2:
        counts[letter] -= 1
        if counts[letter] < 0:
            return False
    return True


def removeDuplicates(string):
    result = []
    seen = set()
    for char in string:
        if char not in seen:
            seen.add(char)
            result.append(char)
    return ''.join(result)


def findMissingNumber1(array1, array2):
    array1.sort()
    array2.sort()
    for num1, num2 in zip(array1, array2):
        if num1 != num2:
            return num1
    return array1[-1]


def findMissingNumber2(array1, array2):
    d = collections.defaultdict(int)
    for num in array2:
        d[num] += 1
    for num in array1:
        if d[num] == 0:
            return num
        else:
            d[num] -= 1


def firstUnique(text):
    counts = collections.defaultdict(int)
    for letter in text:
        counts[letter] += 1
    for letter in text:
        if counts[letter] == 1:
            return letter


def isBalanced(expr):
    if len(expr) % 2 != 0:
        return False
    opening = set('([{')
    match = set([('(', ')'), ('[', ']'), ('{', '}')])
    stack = []
    for char in expr:
        if char in opening:
            stack.append(char)
        else:
            if len(stack) == 0:
                return False
            lastOpen = stack.pop()
            if (lastOpen, char) not in match:
                return False
    return len(stack) == 0


def pairSum1(arr, k):
    if len(arr) < 2:
        return
    arr.sort()
    left, right = (0, len(arr) - 1)
    while left < right:
        currentSum = arr[left] + arr[right]
        if currentSum == k:
            print(arr[left], arr[right])
            left += 1  # or right-=1
        elif currentSum < k:
            left += 1
        else:
            right -= 1


def pairSum2(arr, k):
    if len(arr) < 2:
        return
    seen = set()
    output = set()
    for num in arr:
        target = k - num
        if target not in seen:
            seen.add(num)
        else:
            output.add((min(num, target), max(num, target)))
    print ('\n'.join(map(str, list(output))))


def nextHigher(num):
    strNum = str(num)
    length = len(strNum)
    for i in range(length - 2, -1, -1):
        current = strNum[i]
        right = strNum[i + 1]
        if current < right:
            temp = sorted(strNum[i:])
            next = temp[temp.index(current) + 1]
            temp.remove(next)
            temp = ''.join(temp)
            return int(strNum[:i] + next + temp)
    return num


# 1 2 Fizz ...
fizzBuzz()

# 0 1 1 2 3 5 8 13 21 34 55 89
fib(100)

# False
print(isPrime(6))
# True
print(isPrime(17))

# cei stun
print(removeDuplicates('ceci est un test'))

# True
print(isAnagram1('aabb', 'abab'))
print(isAnagram2('aabb', 'abab'))
# False
print(isAnagram1('aabb', 'aaab'))
print(isAnagram2('aabb', 'aaab'))

a = [0, 1, 2, 3, 4, 5]
b = [4, 2, 3, 0, 1]
# 5
print(findMissingNumber1(a, b))
print(findMissingNumber2(a, b))

# A
print(firstUnique('ooAooBooCoo'))
# True
print(isBalanced('()()[]{{()}}'))
# False
print(isBalanced('(()[([{()}}}'))

k = 5
arr = [4, 2, 5, 3, 1, 0]
# (0, 5) (2, 3) (1, 4)
print(pairSum1(arr, k))
print(pairSum2(arr, k))
# 13245
print(nextHigher(12543))
