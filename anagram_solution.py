import collections
import string
import random
import time


def bubbleSort(str):
    letters = list(str)
    for passn in range(len(letters) - 1, 0, -1):
        for i in range(passn):
            if letters[i] > letters[i + 1]:
                temp = letters[i]
                letters[i] = letters[i + 1]
                letters[i + 1] = temp
    return letters


def isAnagram1(str1, str2):
    return bubbleSort(str1) == bubbleSort(str2)


def isAnagram2(str1, str2):
    return sorted(str1) == sorted(str2)


def isAnagram3(str1, str2):
    if len(str1) != len(str2):
        return False
    counts = collections.defaultdict(int)
    for letter in str1:
        counts[letter] += 1
    for letter in str2:
        counts[letter] -= 1
        if counts[letter] < 0:
            return False
    return True


def randWord(length):
    return ''.join(random.choice(string.ascii_lowercase) for _ in range(length))


def getNextLetter(letter):
    letterIndex = ord(letter) % 96
    newLetterIndex = ((letterIndex + 1) % 26)
    return chr(newLetterIndex + 96)


def getPairWords(isAnagram, length):
    word1 = randWord(length)
    word2 = ''.join(random.sample(word1, length))
    if not isAnagram:  # replace one character by another one
        firstLetter = word1[0]
        word2 = word1.replace(firstLetter, getNextLetter(firstLetter), 1)
    return [word1, word2]


# Prepare some data to run test
iteration = 10
wordLength = 1000
anagramslist, notAnagramslist = list(), list()
for i in range(iteration):
    anagramslist.append(getPairWords(True, wordLength))
    notAnagramslist.append(getPairWords(False, wordLength))

# Run some tests and time the execusions
start = time.time()
for i in range(iteration):
    isAnagram1(anagramslist[i][0], anagramslist[i][1])
    isAnagram1(notAnagramslist[i][0], notAnagramslist[i][1])
end = time.time()
isAnagram1Time = round(end - start, 2)

start = time.time()
for i in range(iteration):
    isAnagram2(anagramslist[i][0], anagramslist[i][1])
    isAnagram2(notAnagramslist[i][0], notAnagramslist[i][1])
end = time.time()
isAnagram2Time = round(end - start, 2)

start = time.time()
for i in range(iteration):
    isAnagram3(anagramslist[i][0], anagramslist[i][1])
    isAnagram3(notAnagramslist[i][0], notAnagramslist[i][1])
end = time.time()
isAnagram3Time = round(end - start, 2)

# Print the results
print('Using bubble sort: ', isAnagram1Time, 'seconds')
print('Using better sort: ', isAnagram2Time, 'seconds')
print('Better algorithm: ', isAnagram3Time, 'seconds')
